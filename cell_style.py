class CellStyle:
    def __init__(self, bold=False):
        self.bold = bold

    def set_bold(self, bold: bool):
        self.bold = bold
        return self

