from enum import Enum


class ColumnType(Enum):
    Position = 0
    Value = 1
    Operation = 2
    Group = 3


class _DisabledCell:
    pass


DisabledCell = _DisabledCell()
