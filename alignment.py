from enum import Enum


class Alignment(Enum):
    Left = "left"
    Center = "center"
    Right = "right"