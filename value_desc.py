from decimal import Decimal
import datetime

from markupsafe import Markup

import lib.utils
from lib.utils.formatters.timedelta_formatter import TimedeltaFormatter
from .alignment import Alignment
from .column_type import DisabledCell
from .ratio import Ratio
from .column_value_type import ColumnValueType


class ValueDesc:
    def __init__(self,
                 type: 'ColumnValueType',
                 hide_zero: bool = None,
                 decimal_points: int = 0,
                 reverse: bool = False,
                 format: str = None,
                 none_display: str = None) -> None:
        self.type = type
        self.none_display = none_display
        if self.none_display is None:
            self.none_display = ""

        # for Number, Currency and Percentage
        self.hide_zero = hide_zero
        self.decimal_points = decimal_points

        # for Diff
        self.reverse = reverse

        # for Date/Duration
        self.format = format

        # for Duration
        DURATION_FORMAT = '{D}d {H}h {M}m {S}s'
        self.duration_formatter = TimedeltaFormatter(self.format or DURATION_FORMAT)

    def format_value(self, value):
        display_value = self._get_display_value(value)

        if display_value is None:
            return self.none_display

        if self.type == ColumnValueType.Number:
            if display_value == 0 and self.hide_zero:
                return ""
            else:
                return "{0:.{points}f}".format(display_value, points=self.decimal_points)

        elif self.type == ColumnValueType.Currency:
            if display_value == 0 and self.hide_zero:
                return ""
            else:
                return lib.utils.format_currency(display_value, self.decimal_points)

        elif self.type == ColumnValueType.Percentage:
            if display_value == 0 and self.hide_zero:
                return ""
            else:
                return "{0:.{points}f}%".format(display_value, points=self.decimal_points)

        elif self.type == ColumnValueType.Diff:
            disp_val = "{0:.{points}f}%".format(display_value * 100, points=self.decimal_points)
            if value == 0:
                return '<span style="color: gray">0% <i class="fa" aria-hidden="true">=</i></span>'
            elif value > 0:
                return '<span style="color: green">{0} <i class="fa fa-arrow-up" aria-hidden="true"></i></span>'.format(
                        disp_val)
            else:
                return '<span style="color: red">{0} <i class="fa fa-arrow-down" aria-hidden="true"></i></span>'.format(
                        disp_val)

        elif self.type == ColumnValueType.Text:
            return display_value

        elif self.type == ColumnValueType.Link:
            return Markup('<a href="{0}">{1}</a>'.format(value[1], value[0]))

        elif self.type == ColumnValueType.Date:
            DATE_FORMAT = "%m/%d/%y"
            return display_value.strftime(self.format or DATE_FORMAT)

        elif self.type == ColumnValueType.Boolean:
            return "Yes" if display_value else "No"

        elif self.type == ColumnValueType.Duration:
            return self.duration_formatter.format(display_value)

        else:
            raise ValueError("no type")

    def get_raw_value(self, value):
        if value is None:
            return None

        if self.type == ColumnValueType.Number or self.type == ColumnValueType.Currency:
            import numpy

            if isinstance(value, Ratio):
                return value.get_raw_value_or_none()
            elif isinstance(value, numpy.int64) or isinstance(value, numpy.float64):
                return value
            elif isinstance(value, int) or isinstance(value, float):
                return value
            elif isinstance(value, Decimal):
                return value
            else:
                raise ValueError("unsupported value type: {0}, {1}".format(type(value), value))
        elif self.type == ColumnValueType.Percentage:
            if isinstance(value, Ratio):
                return value.get_raw_value_or_none()
            elif isinstance(value, int) or isinstance(value, float) or isinstance(value, Decimal):
                return value
            else:
                raise ValueError("unsupported value type: {0}, {1}".format(type(value), value))
        elif self.type in [ColumnValueType.Text, ColumnValueType.Link, ColumnValueType.Date, ColumnValueType.Diff,
                           ColumnValueType.Duration]:
            return value
        elif self.type in [ColumnValueType.Boolean]:
            if isinstance(value, bool):
                return value
            else:
                raise ValueError("unsupported value type: {0}, {1}".format(type(value), value))
        else:
            raise ValueError("no type")

    def get_raw_value_or_default(self, value):
        value = self.get_raw_value(value)
        if value is not None:
            return value

        if self.type in [ColumnValueType.Number, ColumnValueType.Currency, ColumnValueType.Percentage,
                         ColumnValueType.Diff]:
            return 0
        elif self.type in [ColumnValueType.Text, ColumnValueType.Link]:
            return ""
        elif self.type in [ColumnValueType.Date]:
            return datetime.date.min
        elif self.type in [ColumnValueType.Duration]:
            return datetime.timedelta(seconds=0)
        elif self.type in [ColumnValueType.Boolean]:
            return False
        else:
            raise ValueError("no type")

    def get_default_alignment(self):
        if self.type == ColumnValueType.Text:
            return Alignment.Left
        elif self.type == ColumnValueType.Link:
            return Alignment.Left
        elif self.type == ColumnValueType.Number:
            return Alignment.Right
        elif self.type == ColumnValueType.Diff:
            return Alignment.Right
        elif self.type == ColumnValueType.Currency:
            return Alignment.Right
        elif self.type == ColumnValueType.Percentage:
            return Alignment.Right
        elif self.type in (ColumnValueType.Date, ColumnValueType.Duration):
            return Alignment.Center
        elif self.type == ColumnValueType.Boolean:
            return Alignment.Center
        else:
            raise ValueError("no type")

    def can_be_wrapped(self):
        return self.type in [ColumnValueType.Text, ColumnValueType.Link]

    def _get_display_value(self, value):
        if value is None or value is DisabledCell:
            return None

        if self.type == ColumnValueType.Number or self.type == ColumnValueType.Currency:
            import numpy

            if isinstance(value, Ratio):
                return value.get_raw_value_or_none()
            elif isinstance(value, numpy.int64) or isinstance(value, numpy.float64):
                return value
            elif isinstance(value, int) or isinstance(value, float) or isinstance(value, Decimal):
                return value
            else:
                raise ValueError("unsupported value type: {0}, {1}".format(type(value), value))

        elif self.type == ColumnValueType.Percentage:
            if isinstance(value, Ratio):
                return value.get_display_value_or_none()
            elif isinstance(value, int) or isinstance(value, float) or isinstance(value, Decimal):
                return value * 100
            else:
                raise ValueError("unsupported value type: {0}, {1}".format(type(value), value))

        elif self.type in [ColumnValueType.Text, ColumnValueType.Link, ColumnValueType.Date, ColumnValueType.Diff,
                           ColumnValueType.Boolean, ColumnValueType.Duration]:
            return value

        else:
            raise ValueError("no type")

    def is_numeric(self):
        return self.type in (ColumnValueType.Number, ColumnValueType.Currency, ColumnValueType.Percentage)
