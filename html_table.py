from enum import Enum

import colour


class RowStyle(Enum):
    Normal = ""
    NormalEven = "rnormal-row rnormal-row-even"
    NormalOdd = "rnormal-row rnormal-row-odd"
    Separator = "rseparator-row"
    Summary = "rsummary-row"
    SummaryFirst = "rsummary-row-first"
    MinorSummary = "rminorsummary-row"
    MinorSummaryFirst = "rminorsummary-row-first"


class ColumnStyle(Enum):
    Normal = "normal-col"
    Summary = "summary-col"
    SummaryFirst = "summary-col-first"


class SortIcon(Enum):
    NoIcon = 0
    NoSortText = 1
    NoSortNumber = 2
    SortTextAsc = 3
    SortTextDesc = 4
    SortNumberAsc = 5
    SortNumberDesc = 6


class HtmlTableCellStyle:
    def __init__(self):
        self.background_color = None  # type: colour.Color
        self.background_color_alt = None  # type: colour.Color
        self.cursor = None  # type: str
