from enum import Enum
from typing import List, Any


class ReportTableCellEditType(Enum):
    Text = "text"
    TextArea = "textarea"
    List = "list"
    Checkbox = "checkbox"
    Date = "date"
    Currency = "currency"


class ReportTableCellEditInfo:
    def __init__(self, type: ReportTableCellEditType, action: str) -> None:
        self.type = type
        self.action = action
        self.multiline = False
        self.items = []  # type: List[Any]
        self.allow_custom = False

    def add_item(self, value, display, *, color: str = None):
        self.items.append((value, display, color))

    def get_data(self):
        if self.type == ReportTableCellEditType.List:
            return {
                "items": self.items,
                "allow_custom": self.allow_custom,
            }
