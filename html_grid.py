from typing import List, Dict

import io
import re
from enum import Enum
from markupsafe import Markup
from dominate.tags import *
from openpyxl import Workbook
from openpyxl.cell import Cell
from openpyxl.styles import Font
from openpyxl.utils import get_column_letter

from lib.html_tables.cell_style import CellStyle
from lib.html_tables.column_value_type import ColumnValueType
from lib.html_tables.value_desc import ValueDesc
from .alignment import Alignment


class HtmlGridCellStyle(Enum):
    Normal = "rnormal"
    HeaderLevel1 = "rheader1"
    HeaderLevel2 = "rheader2"


class HtmlGridCellBorderPosition(Enum):
    Top = "border-top"
    Bottom = "border-bottom"
    Left = "border-left"
    Right = "border-right"


class HtmlGridCellBorderDesc:
    def __init__(self, width: int = None):
        self.width = width


class HtmlGridCell:
    def __init__(self, value, value_desc: ValueDesc,
                 style: HtmlGridCellStyle, row_span: int, col_span: int, width: int,
                 min_width: int = None,
                 alignment: Alignment = None, link: str = None, hint: str = None,
                 cell_style: CellStyle = CellStyle()) -> None:
        self.value = value
        self.value_desc = value_desc
        self.style = style
        self.row_span = row_span
        self.col_span = col_span
        self.width = width
        self.min_width = min_width
        self.alignment = alignment
        self.link = link
        self.hint = hint
        self.cell_style = cell_style

        self.borders: Dict[HtmlGridCellBorderPosition, HtmlGridCellBorderDesc] = {
            HtmlGridCellBorderPosition.Top: HtmlGridCellBorderDesc(),
            HtmlGridCellBorderPosition.Bottom: HtmlGridCellBorderDesc(),
            HtmlGridCellBorderPosition.Left: HtmlGridCellBorderDesc(),
            HtmlGridCellBorderPosition.Right: HtmlGridCellBorderDesc(),
        }

    def set_border(self, position: HtmlGridCellBorderPosition, *, width: int):
        self.borders[position].width = width


class HtmlGridRow:
    def __init__(self, cls: str = None) -> None:
        self.cells = []  # type: List[HtmlGridCell]
        self.cls = cls

    def add_header(self, text: str, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                   header_level: int = 1,
                   alignment: Alignment = None, link: str = None, hint: str = None, style=CellStyle()):
        hl = {
            1: HtmlGridCellStyle.HeaderLevel1,
            2: HtmlGridCellStyle.HeaderLevel2,
        }
        value_desc = ValueDesc(ColumnValueType.Text)
        cell = HtmlGridCell(text, value_desc, hl[header_level], row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_empty_cell(self, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                       alignment: Alignment = None,
                       link: str = None, hint: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Text)
        cell = HtmlGridCell(None, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_text(self, value: str, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                      alignment: Alignment = None, link: str = None, hint: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Text)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_number(self, value, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                        alignment: Alignment = None, link: str = None, hint: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Number, hide_zero=False)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_percentage(self, value, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                            alignment: Alignment = None, decimal_points: int = 0, link: str = None, hint: str = None,
                            style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Percentage, hide_zero=False, decimal_points=decimal_points)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_duration(self, value, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                          alignment: Alignment = None, format: str = None,
                          link: str = None, hint: str = None, none_display: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Duration, hide_zero=False, format=format,
                               none_display=none_display)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_currency(self, value, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                          alignment: Alignment = None, decimal_points=0,
                          link: str = None, hint: str = None, none_display: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Currency, hide_zero=False, decimal_points=decimal_points,
                               none_display=none_display)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def add_cell_date(self, value, row_span: int = 1, col_span: int = 1, width: int = None, min_width: int = None,
                      alignment: Alignment = None,
                      link: str = None, hint: str = None,
                      none_display: str = None, style=CellStyle()):
        value_desc = ValueDesc(ColumnValueType.Date, none_display=none_display)
        cell = HtmlGridCell(value, value_desc, HtmlGridCellStyle.Normal, row_span=row_span, col_span=col_span,
                            min_width=min_width, width=width, alignment=alignment, link=link, hint=hint,
                            cell_style=style)
        self.cells.append(cell)
        return self

    def all_headers(self):
        return all(x.style in [HtmlGridCellStyle.HeaderLevel1, HtmlGridCellStyle.HeaderLevel2] for x in self.cells)

    def set_header(self, header_level: int = 1):
        hl = {
            1: HtmlGridCellStyle.HeaderLevel1,
            2: HtmlGridCellStyle.HeaderLevel2,
        }
        for c in self.cells:
            c.style = hl[header_level]

    def set_border(self, position: HtmlGridCellBorderPosition, *, width: int):
        for c in self.cells:
            c.set_border(position, width=width)


class HtmlGrid:
    def __init__(self, cls: str = None):
        self.cls = cls
        self.rows = []  # type: List[HtmlGridRow]
        self.widths = []  # type: List[int]
        self.table_width = None  # type: int

    def insert_row(self, idx: int) -> HtmlGridRow:
        row = HtmlGridRow()
        self.rows.insert(idx, row)
        return row

    def add_row(self, cls: str = None) -> HtmlGridRow:
        row = HtmlGridRow(cls=cls)
        self.rows.append(row)
        return row

    def set_widths(self, widths: List[int]):
        self.widths = widths

    def set_width(self, width: int):
        self.table_width = width

    def __str__(self):
        return str(self.render())

    def render(self):
        classes = ["rgrid"]
        if self.cls is not None:
            classes.append(self.cls)
        doc = table(cls=" ".join(classes))

        with doc:
            if self.table_width is not None:
                attr(width=self.table_width)

            if self.widths is not None:
                for w in self.widths:
                    col(width=w)

            with tbody():
                for row_idx, row in enumerate(self.rows):
                    with tr():
                        if row.cls is not None:
                            attr(cls=row.cls)

                        for cell in row.cells:
                            self._emit_cell(row_idx, row, cell)

        return Markup(doc)

    def create_xlsx_into_worksheet(self, ws):
        col_widths = {}

        for row_idx, row in enumerate(self.rows):
            cols = []
            for col_idx, cell in enumerate(row.cells):
                cols.append(self._create_xlsx_cell(ws, col_idx, row_idx, col_widths, row, cell))
            ws.append(cols)

        for col_idx, width in col_widths.items():
            ws.column_dimensions[get_column_letter(col_idx + 1)].width = width / 72 * 10

    def create_xlsx(self):
        wb = Workbook()
        ws = wb.active
        ws.title = "Data"

        self.create_xlsx_into_worksheet(ws)

        data = io.BytesIO()
        wb.save(data)
        return data.getvalue()

    def _emit_cell(self, row_idx: int, row: HtmlGridRow, cell: HtmlGridCell):
        cls = [cell.style.value]

        if cell.hint is not None:
            cls.append("show-tooltip")

        val = cell.value_desc.format_value(cell.value)
        alignment = cell.alignment
        if alignment is None:
            if row_idx == 0 and row.all_headers():
                alignment = Alignment.Center
            else:
                alignment = cell.value_desc.get_default_alignment()

        val = str(Markup.escape(val))
        val = re.sub(" {2,}", lambda x: "&nbsp;" * len(x.group(0)), val)
        val = val.replace("\n", "<br/>")
        with td(colspan=cell.col_span, rowspan=cell.row_span, cls=" ".join(cls), align=alignment.value) as el:

            if cell.link is None:
                el.add_raw_string(val)
            else:
                with a(href=cell.link) as el2:
                    el2.add_raw_string(val)

            if cell.width:
                attr(width=cell.width)
            if cell.min_width is not None:
                attr(style="min-width: {0}px".format(cell.min_width))

            styles = []
            if cell.cell_style.bold:
                styles.append("font-weight: bold")

            for b, d in cell.borders.items():
                if d.width is not None:
                    styles.append("{}-width: {}px".format(b.value, d.width))

            attr(style="; ".join(styles))

            if cell.hint is not None:
                attr(title=cell.hint)
                el.add_raw_string("""<i class="fa fa-info info-hint" aria-hidden="true"></i>""")

    def _create_xlsx_cell(self, ws, col_idx, row_idx, cols_widths, row, cell):
        import openpyxl.styles

        if cell.value_desc.is_numeric():
            val = cell.value_desc.get_raw_value(cell.value)
        else:
            val = cell.value_desc.format_value(cell.value)
        alignment = cell.alignment
        if alignment is None:
            if row_idx == 0 and row.all_headers():
                alignment = Alignment.Center
            else:
                alignment = cell.value_desc.get_default_alignment()

        if cell.width is not None:
            if col_idx not in cols_widths or cell.width > cols_widths[col_idx]:
                cols_widths[col_idx] = cell.width

        alignment = openpyxl.styles.Alignment(horizontal=alignment.value,
                                              vertical='bottom',
                                              text_rotation=0,
                                              wrap_text=False,
                                              shrink_to_fit=False,
                                              indent=0)

        bold = cell.cell_style.bold
        if cell.style == HtmlGridCellStyle.HeaderLevel1:
            bold = True

        xcell = Cell(ws, value=val)
        xcell.alignment = alignment
        xcell.font = Font(size=10, bold=bold)
        return xcell
