export function install() {
  $(window).bind("scroll", checkScroll);
  $(window).bind("resize", checkScroll);
}

export function update() {
  checkScroll();

  let table = $("#table");
  console.log(table);

  let w = table.data("table-data") || "{}";
  console.log("tableData", w);

  let freezeHeader = w["freeze_header"];
  let freezeColumnNum = w["freeze_column"];

  // header
  if (freezeHeader) {
    let clone = table.clone();
    clone.attr("id", "table-fixed-header");
    $("tbody", clone).remove();
    clone.insertAfter(table);
  }

  // column
  if (freezeColumnNum != null) {
    let clone = table.clone();
    clone.attr("id", "table-fixed-columns");
    clone.css("width", "1px");
    clone.removeAttr("width");

    $("thead tr:first th", clone).slice(freezeColumnNum).remove();
    $("thead tr:gt(0) th", clone).remove();

    $("tbody tr", clone).each(function () {
      let tr = $(this);
      $("td", tr).slice(freezeColumnNum).remove();
    });
    clone.insertAfter(table);
  }

  // corner
  if (freezeHeader && freezeColumnNum != null) {
    let clone = table.clone();
    clone.attr("id", "table-fixed-corner");
    clone.removeAttr("width");

    $("thead tr:first th", clone).slice(freezeColumnNum).remove();
    $("thead tr:gt(0) th", clone).remove();
    $("tbody", clone).remove();
    clone.insertAfter(table);
  }
}

function checkScroll() {
  let table = $("#table");

  if (table.length == 0)
    return;

  let w = table.data("table-data") || "{}";
  // console.log("tableData", w);

  let freezeHeader = w["freeze_header"];
  let freezeColumnNum = w["freeze_column"];

  var tableOffsetAbs = table.offset();
  var fixedHeader = $("#table-fixed-header");
  var fixedColumns = $("#table-fixed-columns");
  var fixedCorner = $("#table-fixed-corner");

  let mainContainer = $("#main-container");

  if (mainContainer.length == 0)
    return;

  var tableOffsetRel = { top: table[0].offsetTop, left: table[0].offsetLeft };

  let contTop = mainContainer.offset().top - 16;
  let contLeft = mainContainer.offset().left;

  var scrollT = $(window).scrollTop();
  var scrollL = $(window).scrollLeft();

  let showCorner = 0;

  let topPos = (tableOffsetRel.top - tableOffsetAbs.top) + scrollT + contTop;
  let leftPos = (tableOffsetRel.left - tableOffsetAbs.left) + scrollL + contLeft;

  if (fixedHeader.length != 0) {
    if (scrollT + contTop >= tableOffsetAbs.top) {

      var tableWidth = table[0].offsetWidth;
      fixedHeader.css("top", `${topPos}px`);
      fixedHeader.css("left", "0px");
      fixedHeader.css("width", tableWidth + "px");

      showCorner++;
      if (fixedHeader.is(":hidden"))
        fixedHeader.show();
    }
    else if (scrollT < tableOffsetAbs.top) {
      fixedHeader.hide();
    }
  }

  if (fixedColumns.length != 0) {
    if (scrollL + contLeft >= tableOffsetAbs.left) {
      showCorner++;

      fixedColumns.css("top", `${tableOffsetRel.top }px`);
      fixedColumns.css("left", `${leftPos}px`);
      if (fixedColumns.is(":hidden"))
        fixedColumns.show();
    }
    else if (scrollL < tableOffsetAbs.left) {
      fixedColumns.hide();
    }
  }

  if (fixedCorner.length != 0) {
    if (showCorner == 2) {
      fixedCorner.css("top", `${topPos}px`);
      fixedCorner.css("left", `${leftPos}px`);
      if (fixedCorner.is(":hidden"))
        fixedCorner.show();
    }
    else {
      fixedCorner.hide();
    }
  }

  let mainRows = $("thead tr", table).toArray();
  let fixedColumnsRows = $("thead tr", fixedColumns).toArray();
  let fixedCornerRows = $("thead tr", fixedCorner).toArray();
  // console.log(mainRows, fixedColumnsRows, fixedCornerRows);

  let i = 0;
  for (let tr of mainRows) {
    if (fixedColumnsRows.length > i)
      fixedColumnsRows[i].setAttribute("height", `${tr.offsetHeight}`);
    if (fixedCornerRows.length > i)
      fixedCornerRows[i].setAttribute("height", `${tr.offsetHeight}`);
    i++;
  }

  let rowIdx = 1;
  for (let tr of mainRows) {
    let colIdx = 1;
    for (let col of $("th", tr).toArray()) {
      let w = col.offsetWidth;

      //console.log(`Set ${rowIdx}:${colIdx} to ${w}px`);

      $(`thead > tr:nth-child(${rowIdx}) > th:nth-child(${colIdx})`, fixedHeader).attr("width", `${w}`);

      if (colIdx <= freezeColumnNum)
         $(`thead > tr:nth-child(${rowIdx}) > th:nth-child(${colIdx})`, fixedColumns).attr("width", `${w}`);
      //$(`thead > tr:nth-child(${rowIdx}) > th:nth-child(${colIdx})`, fixedCorner).attr("width", `${w}`);

      colIdx++;
    }

    rowIdx++;
  }

  let dataRows = $("tbody tr", table).toArray();
  let fixedColumnsDataRows = $("tbody tr", fixedColumns).toArray();

  for (let i = 0; i < dataRows.length; i++) {
    if (fixedColumnsDataRows[i])
      fixedColumnsDataRows[i].setAttribute("height", `${dataRows[i].offsetHeight}`);
  }
}
