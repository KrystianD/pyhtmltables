import { parseUSDate } from "./utils";

declare function unescape(s: string): string;

function getCookie(name) {
  let re = new RegExp(name + "=([^;]+)");
  let value = re.exec(document.cookie);
  return (value != null) ? unescape(value[1]) : null;
}

export class TableEditableCell {
  obj: JQuery;
  textEl: JQuery;
  editIconEl: JQuery;
  loaderEl: JQuery;
  editWrapEl: JQuery;
  editInputEl: JQuery;
  errorIconEl: JQuery;
  type: string;
  visibleIconEl: JQuery;
  prepared: boolean = false;
  isShown: boolean = false;

  linkEl: JQuery;

  onReloadRequested = null;

  globalConfig: any;
  listItems;
  allowCustom: boolean;

  // list input
  editListDropdownInputEl: JQuery;

  // checkbox
  editCheckboxEl: JQuery;

  constructor(obj: JQuery) {
    this.obj = obj;
    this.type = obj.data("type");
    this.textEl = $("<pre>");
    this.linkEl = $("<a>");
    this.editIconEl = $('<i class="fa fa-pencil" aria-hidden="true"></i>');
    this.errorIconEl = $('<i class="fa fa-exclamation-triangle" style="color: red" aria-hidden="true"></i>');
    this.loaderEl = $(`<img class="loader" src="${window["BASE_URL"]}/static/spinner1.gif" width="14" />`);
  }

  private isAutoUpdate() {
    return this.obj.data("update") || false;
  }

  public install() {
    let value = JSON.parse(this.obj.text().trim());
    this.obj.text("");

    if (this.type == "text-link") {
      this.linkEl = $("<a>").addClass("hideable");
      this.textEl.append(this.linkEl);

    } else if (this.type === "list") {
      let globalDataEl = this.obj.parents("*[data-editables]");
      if (globalDataEl.length > 0) {
        let globalData = globalDataEl.data("editables");
        this.globalConfig = globalData[this.obj.data("column-tag")];
        this.listItems = this.globalConfig["items"];
        this.allowCustom = this.globalConfig["allow_custom"];
      }
      else {
        this.listItems = this.obj.data("items");
      }

    } else if (this.type === "checkbox") {
      this.editIconEl.hide();

      this.obj.parents("td").addClass("select-checkbox-on-click");

      let el = $(`<div class="checkbox checkbox-primary" />`);
      this.editCheckboxEl = $(`<input type="checkbox" />`);
      el.append(this.editCheckboxEl);
      el.append("<label/>");

      this.textEl = el;

      this.editCheckboxEl.click(() => this.saveEdit(this.editCheckboxEl.is(":checked")));

      if (value === true)
        this.editCheckboxEl.attr("checked", "checked");

      this.prepareElemenet();
    }

    this.setValue(value);

    this.obj.empty();
    this.obj.append(this.textEl);
    this.obj.append(this.editIconEl);
    this.visibleIconEl = this.editIconEl;

    this.editIconEl.click(() => this.startEdit());
  }

  // called on edit start
  private prepareElemenet() {
    if (this.prepared)
      return;
    this.prepared = true;

    this.editWrapEl = $("<div class='itemwrap'>");
    if (this.type == "text" || this.type == "currency") {
      this.editInputEl = $("<input type='text'/>");
      this.editInputEl.focus(() => this.editInputEl.select());
      this.editInputEl.blur(() => this.saveEdit(this.editInputEl.val()));
    }
    else if (this.type == "text-link") {
      this.editInputEl = $("<input type='text'/>");
      this.editInputEl.blur(() => this.saveEdit(this.editInputEl.val()));
    }
    else if (this.type == "date") {
      this.editInputEl = $("<input type='text'/>");
      (this.editInputEl as any).datepicker({
        format: "mm/dd/yyyy",
        clearBtn: true,
        todayBtn: "linked",
        weekStart: 1,
      }).on("hide", () => this.saveEdit(this.editInputEl.val()));
    }
    else if (this.type == "textarea") {
      this.editInputEl = $("<textarea></textarea>");
      this.editInputEl.blur(() => this.saveEdit(this.editInputEl.val()));
    }
    else if (this.type == "list") {
      let html = `<div class="dropdown" tabindex="1"><ul class="dropdown-menu"></ul></div>`;
      this.editInputEl = $(html);
      this.editInputEl.addClass("open");

      this.editInputEl.focus();
      this.editInputEl.focusout((e: Event) => {
        let rel = (e as any).relatedTarget;
        if (this.editInputEl.has(rel).length > 0 || this.editInputEl[0] == rel) {
          e.preventDefault();
        } else {
          this.cancelEdit();
        }
      });

      let ulEl = this.editInputEl.find("ul");
      for (let item of this.listItems) {
        let name = item[0];
        let display = item[1];
        let color = item[2];

        let el = $("<a/>").attr("data-name", name).text(display);
        el.css("color", color || "");
        el.click(() => {
          this.saveEdit(name);
        });

        ulEl.append($("<li/>").append(el));
      }

      if (this.allowCustom) {
        let editSaveEvent = () => {
          this.saveEdit(this.editListDropdownInputEl.val());
        };

        this.editListDropdownInputEl = $(`<input type='text'/>`);
        this.editListDropdownInputEl.keydown((e) => {
          var code = e.keyCode || e.which;
          if (code == 13)
            this.saveEdit(this.editListDropdownInputEl.val());
        });
        let aEl = $(`<a class="save">Save</a>`).click(editSaveEvent);

        ulEl.append($(`<li role="separator" class="divider"></li>`));
        ulEl.append($(`<li class="dropdown-header">Custom:</li>`));
        ulEl.append($("<li/>").append(this.editListDropdownInputEl));
        ulEl.append($("<li/>").append(aEl));
      }
    } else if (this.type === "checkbox") {
      this.loaderEl.addClass("absolute-pos");
    }

    if (this.type !== "checkbox") {
      this.errorIconEl.click(() => this.startEdit());
    }

    this.editWrapEl.append(this.editInputEl);
  }

  private startEdit() {
    if (this.isShown)
      return;

    this.prepareElemenet();

    this.editInputEl.val(this.getCurrentValue());

    if (this.type == "date") {
      var dt = parseUSDate(this.getCurrentValue());
      (this.editInputEl as any).datepicker("setDate", dt);
    } else if (this.type == "list") {
      let value = this.getCurrentValue();
      this.editInputEl.find("a").css("font-weight", "");

      if (this.allowCustom) {
        this.editListDropdownInputEl.val("");
      }
      if (value == null) {
        this.editInputEl.find("a:not([data-name]):not(.save)").css("font-weight", "bold");
      }
      else {
        let el = this.editInputEl.find(`a[data-name="${value}"]`);
        if (el.length > 0) {
          el.css("font-weight", "bold");
        } else {
          if (this.allowCustom) {
            this.editListDropdownInputEl.val(value);
          }
        }
      }
    }

    this.textEl.before(this.editWrapEl).detach();
    this.editInputEl.focus();

    this.isShown = true;
  }

  public saveEdit(value: string | boolean, dontReload = false) {
    let oldValue = null;
    if (this.type !== "checkbox") {
      oldValue = this.getCurrentValue();
    }

    if (this.type === "checkbox") {
      this.editCheckboxEl.attr("disabled", "disabled");
    }

    if (this.type == "currency") {
      value = (value as string).replace(/,/g, ".");
    }

    console.log("saveEdit", oldValue, "new", value);

    this.setValue(value as string);

    this.cancelEdit();
    if (value != oldValue) {
      this.visibleIconEl.before(this.loaderEl).detach();

      let data = this.obj.data();
      let sendData = {};
      for (let key in data)
        if (key != "editable")
          sendData[key] = data[key];
      sendData["value"] = JSON.stringify(value);
      console.log("data", sendData);

      let sessionToken = getCookie("remember_token");
      $.post({
        url: window["BASE_URL"] + this.obj.data("action") + `?session_token=${sessionToken}`,
        data: sendData,
        success: (data) => {
          console.log(data);
          let res = data["result"];
          if (res == "ok") {
            this.visibleIconEl = this.editIconEl;
            this.loaderEl.before(this.visibleIconEl).detach();

            if ((this.isAutoUpdate() || data["need_reload"]) && !dontReload)
              if (this.onReloadRequested !== null)
                this.onReloadRequested();

            let msg = data["message"];
            if (msg)
              alert(msg);

          } else {
            this.setValue(oldValue);
            this.visibleIconEl = this.errorIconEl;
            this.loaderEl.before(this.visibleIconEl).detach();
            throw "api-error";
          }

          if (this.type === "checkbox") {
            this.editCheckboxEl.removeAttr("disabled");
          }
        },
        error: () => {
          this.setValue(oldValue);
          this.visibleIconEl = this.errorIconEl;
          this.loaderEl.before(this.visibleIconEl).detach();

          if (this.type === "checkbox") {
            this.editCheckboxEl.removeAttr("disabled");
          }
        }
      });
    }
  }

  private cancelEdit() {
    if (!this.isShown)
      return;
    this.isShown = false;

    this.editWrapEl.before(this.textEl).detach();
  }

  private setValue(value: string) {
    if (this.type == "list") {
      this.textEl.text(this.getItemDisplay(value));
      const color = this.getItemColor(value);
      this.textEl.css("color", color || "");
    }
    else if (this.type == "text-link") {
      this.linkEl.text(value);
      let w = this.obj.data("width");
      this.linkEl.css("width", `${w}px`);
    }
    else if (this.type == "checkbox") {
    }
    else if (this.type == "currency") {
      this.textEl.text("$" + value);
    }
    else {
      this.textEl.text(value);
    }
  }

  private getCurrentValue(): string {
    let value = this.textEl.text();
    if (this.type == "list") {
      return this.getItemName(value);
    }
    else if (this.type == "text-link") {
      return this.linkEl.text();
    }
    else if (this.type == "checkbox") {
      throw "no";
    }
    else if (this.type == "currency") {
      value = value.replace(/\$/g, "");
      return value;
    }
    else {
      return value;
    }
  }

  private getItemDisplay(sname: string): string {
    for (let item of this.listItems) {
      let name = item[0];
      let display = item[1];
      if (name == sname)
        return display;
    }
    if (sname == null)
      return "(none)";

    if (this.allowCustom)
      return sname;

    throw `no-item ${sname}`;
  }

  private getItemColor(sname: string): string {
    for (let item of this.listItems) {
      let name = item[0];
      let color = item[2];
      if (name == sname)
        return color;
    }
    if (sname == null)
      return null;

    if (this.allowCustom)
      return sname;

    return null;
  }

  private getItemName(sdisplay: string): string {
    for (let item of this.listItems) {
      let name = item[0];
      let display = item[1];
      if (display == sdisplay)
        return name;
    }
    if (sdisplay == "(none)")
      return null;

    if (this.allowCustom)
      return sdisplay;

    throw "no-display";
  }
}
