from typing import List, TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from .row import ReportTableRow
    from .summary import ReportTableSummaryRowDesc


class ReportTableRowsGroup:
    def __init__(self, title: Optional[str]) -> None:
        self.title = title
        self.rows = []  # type: List[ReportTableRow]
        self.summary_rows = []  # type: List[ReportTableSummaryRowDesc]

    def get_rows(self) -> List['ReportTableRow']:
        return self.rows
