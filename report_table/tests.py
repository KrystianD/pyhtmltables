import subprocess
import time

import tempfile

from lib.html_tables.report_table import ReportTable
from services.Reports.lib.mail_sender import combine_html


def main():
    table = ReportTable()

    table.add_column_text("A")
    g = table.add_column_group("GR")
    g.add_column_text("A")
    g.add_column_text("A")
    g.add_column_text("A")
    g2 = g.add_column_group("GR")
    g2.add_column_text("A")
    g2.add_column_text("A")
    g.border = True
    table.add_column_text("A")

    table.new_row()
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.new_row()
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")
    table.add_cell("A")

    table.set_footer("asdasd")

    html = combine_html(table.render())

    f = tempfile.NamedTemporaryFile(suffix=".html")
    f.write(html.encode("utf-8"))
    f.flush()
    files = []
    files.append(f.name)

    subprocess.call(["chromium"] + files)
    time.sleep(5)


main()
