from typing import List, TYPE_CHECKING, Any, Tuple, Dict

from lib.html_tables.alignment import Alignment
from lib.html_tables.column_type import ColumnType
from lib.html_tables.column_value_type import ColumnValueType
from lib.html_tables.edit_info import ReportTableCellEditInfo
from lib.html_tables.value_desc import ValueDesc

if TYPE_CHECKING:
    from .conditional_formatters import IConditionalFormatter


class ColumnEntry:
    def __init__(self, overlap: 'ReportTableColumn' = None, user: 'ReportTableColumn' = None) -> None:
        self.overlap = overlap
        self.user = user

    def __repr__(self):
        return "{0}:{1}".format(self.overlap, self.user)


class ReportTableOperationButton:
    def __init__(self, action: str) -> None:
        self.tag = None  # type: str
        self.link = None  # type: str
        self.name = None  # type: str
        self.action = action
        self.confirm = False
        self.prompt = False

    def set_confirm(self, confirm: bool):
        self.confirm = confirm
        return self

    def set_prompt(self, prompt: str):
        self.prompt = prompt
        return self

    def set_tag(self, tag: str):
        self.tag = tag
        return self

    def set_link(self, link: str):
        self.link = link
        return self


class ReportTableColumn:
    def __init__(self,
                 type: ColumnType = None,
                 name: str = None,
                 parent: 'ReportTableColumn' = None,
                 hint: str = None,
                 tag: Any = None,
                 width: int = None,
                 min_width: int = None,
                 value_desc: 'ValueDesc' = None,
                 alignment: Alignment = None,
                 header_alignment=Alignment.Center,
                 formatter: 'IConditionalFormatter' = None,
                 sortable: bool = False,
                 edit_info: ReportTableCellEditInfo = None) -> None:

        self.type = type
        self.name = name
        self.parent = parent
        self.hint = hint
        self.tag = tag
        self.alignment = alignment
        self.header_alignment = header_alignment
        self.width = width
        self.min_width = min_width
        self.value_desc = value_desc
        self.formatter = formatter
        self.sortable = sortable
        self.freezed = False
        self.is_summary_style = False
        self.is_summary_col = False
        self.edit_info = edit_info
        self.border = False

        self.columns = []  # type: List[ReportTableColumn]

        # operations
        self.operations = []  # type: List[ReportTableOperationButton]

    def freeze(self):
        self.freezed = True
        return self

    def enable_border(self):
        self.border = True
        return self

    def is_summary(self):
        return self.is_summary_col or (self.parent is not None and self.parent.is_summary())

    def is_summary_style2(self):
        return self.is_summary() or self.is_summary_style or (
                self.parent is not None and self.parent.is_summary_style2())

    def format_value(self, value):
        from lib.html_tables.report_table.cell import ReportTableCell
        if isinstance(value, ReportTableCell):
            return self.value_desc.format_value(value.value)
        else:
            return self.value_desc.format_value(value)

    def get_raw_value(self, value):
        from lib.html_tables.report_table.cell import ReportTableCell
        if isinstance(value, ReportTableCell):
            return self.value_desc.get_raw_value(value.value)
        else:
            return self.value_desc.get_raw_value(value)

    def is_numeric(self):
        return self.value_desc.is_numeric()

    def add_column_group(self, name, hint=None):
        col = ReportTableColumn(name=name, hint=hint, type=ColumnType.Group)
        self.columns.append(col)
        return col

    def add_column_pos(self, name="#", hint=None, tag=None, alignment=Alignment.Center, width=50,
                       min_width=None) -> 'ReportTableColumn':
        col = ReportTableColumn(name=name, type=ColumnType.Position, parent=self, hint=hint, tag=tag,
                                alignment=alignment, width=width, min_width=min_width)
        self.columns.append(col)
        return col

    def add_column_text(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                        edit_info: ReportTableCellEditInfo = None) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Text)
        col = ReportTableColumn(name=name, type=ColumnType.Value,
                                parent=self, hint=hint, tag=tag, alignment=alignment, width=width, min_width=min_width,
                                value_desc=desc,
                                sortable=sortable, edit_info=edit_info)
        self.columns.append(col)
        return col

    def add_column_boolean(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                           edit_info: ReportTableCellEditInfo = None) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Boolean)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                value_desc=desc,
                                sortable=sortable,
                                edit_info=edit_info)
        self.columns.append(col)
        return col

    def add_column_date(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                        format: str = None,
                        formatter: 'IConditionalFormatter' = None,
                        edit_info: ReportTableCellEditInfo = None) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Date, format=format)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=desc,
                                sortable=sortable,
                                edit_info=edit_info)
        self.columns.append(col)
        return col

    def add_column_number(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, hide_zero=True,
                          decimal_points=0,
                          formatter: 'IConditionalFormatter' = None,
                          sortable=False, edit_info: ReportTableCellEditInfo = None) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Number, hide_zero=hide_zero, decimal_points=decimal_points)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width, formatter=formatter,
                                value_desc=desc,
                                sortable=sortable, edit_info=edit_info)
        self.columns.append(col)
        return col

    def add_column_currency(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, hide_zero=True,
                            decimal_points=0,
                            formatter: 'IConditionalFormatter' = None, sortable=False,
                            edit_info: ReportTableCellEditInfo = None) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Currency, hide_zero=hide_zero, decimal_points=decimal_points)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=desc,
                                sortable=sortable, edit_info=edit_info)
        self.columns.append(col)
        return col

    def add_column_percentage(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                              hide_zero=True, decimal_points=0,
                              formatter: 'IConditionalFormatter' = None, sortable=False) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Percentage, hide_zero=hide_zero, decimal_points=decimal_points)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=desc,
                                sortable=sortable)
        self.columns.append(col)
        return col

    def add_column_duration(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                            hide_zero=True, format: str = None,
                            formatter: 'IConditionalFormatter' = None, sortable=False) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Duration, hide_zero=hide_zero, format=format)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=desc,
                                sortable=sortable)
        self.columns.append(col)
        return col

    def add_column_diff(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, decimal_points=0,
                        formatter: 'IConditionalFormatter' = None, reverse=False) -> 'ReportTableColumn':
        desc = ValueDesc(type=ColumnValueType.Diff, decimal_points=decimal_points, reverse=reverse)
        col = ReportTableColumn(name=name, type=ColumnType.Value, parent=self, hint=hint, tag=tag, alignment=alignment,
                                width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=desc, sortable=False)
        self.columns.append(col)
        return col

    def add_column_operations(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                              formatter: 'IConditionalFormatter' = None) -> 'ReportTableColumn':
        col = ReportTableColumn(name=name, type=ColumnType.Operation, parent=self, hint=hint, tag=tag,
                                alignment=alignment, width=width, min_width=min_width,
                                formatter=formatter,
                                value_desc=None, sortable=False)
        self.columns.append(col)
        return col

    def get_columns(self) -> List['ReportTableColumn']:
        return self.columns

    def is_fixed_width(self):
        return self.width is not None

    def all_subcolumns_empty(self):
        return all(x.name is None for x in self.columns)

    def is_column_group(self):
        return len(self.columns) > 0

    def is_single(self):
        return len(self.columns) == 1

    def get_all_columns(self) -> List['ReportTableColumn']:
        cols = []
        for c in self.columns:
            cols.append(c)
            cols += c.get_all_columns()
        return cols

    def get_col_width(self):
        if self.is_column_group():
            return sum(x.get_col_width() for x in self.columns)
        else:
            return 1

    def get_depth(self):
        if self.is_column_group():
            return max(x.get_depth() for x in self.columns) + 1
        else:
            return 0

    def get_leveled(self) -> Tuple[List[List[Tuple['ReportTableColumn', int]]], Any, Any, Any, Any]:
        cols_matrix = []  # type: List[List[ColumnEntry]]
        all_columns = self.get_all_columns()
        # print(all_columns)
        row_spans = {x: 1 for x in all_columns}
        col_spans = {x: 1 for x in all_columns}

        # set columns to proper positions in matrix
        width = self.get_col_width()
        depth = self.get_depth()
        cols_per_level = []  # type: List[List[Tuple[ReportTableColumn,int]]]
        for level in range(depth):
            cols_matrix.append([ColumnEntry() for _ in range(width)])
            cols_per_level.append([])
        self._process_columns_levels(cols_matrix, cols_per_level, 0, 0)

        # self.print()
        # print("mat1")
        # pprint.pprint(cols_matrix[0])
        # pprint.pprint(cols_matrix[1])

        # move cells from bottom to top
        for level in range(depth - 1, 0, -1):
            for col_idx, col_entry in enumerate(cols_matrix[level]):
                if col_entry.overlap is None:
                    continue
                col_above = cols_matrix[level - 1][col_idx]
                # print(col_idx, col, below)
                if col_above.overlap is None:
                    col_above.overlap = col_entry.overlap
                    col_above.user = col_entry.user

        # print("mat2")
        # pprint.pprint(cols_matrix[0])
        # pprint.pprint(cols_matrix[1])

        # fill empty cells from top to bottom
        for level in range(depth - 1):
            # print("A", i)
            # pprint.pprint(cols_matrix)
            # row_proceed = {}
            for col_idx, col_entry in enumerate(cols_matrix[level]):
                if col_entry.overlap is None:
                    continue
                col_below = cols_matrix[level + 1][col_idx]
                # print(col_idx, col, below)
                if col_below.overlap is None:
                    col_below.overlap = col_entry.overlap
                if col_below.user is None:
                    col_below.user = col_entry.user
                    # if col_entry.overlap not in row_proceed:
                    #    row_spans[col_entry.overlap] += 1
                    #    row_proceed[col_entry.overlap] = True

        # print("mat3")
        # pprint.pprint(cols_matrix[0])
        # pprint.pprint(cols_matrix[1])

        end_columns = []
        for col in cols_matrix[depth - 1]:
            end_columns.append(col.user or col.overlap)  # or overlap for single column-groups

        added_cols = {}  # type: Dict[ReportTableColumn,bool]
        for level in range(depth):
            col_idx = 0
            while col_idx < len(cols_matrix[level]):
                c = cols_matrix[level][col_idx]

                if c.overlap not in added_cols:
                    # print("col", i, c.user)

                    for y in range(level + 1, depth):
                        c_bottom = cols_matrix[y][col_idx]
                        if c_bottom.overlap == c.overlap:
                            row_spans[c_bottom.overlap] += 1
                        else:
                            break

                    cols_per_level[level].append((c.user, col_idx))
                    added_cols[c.overlap] = True
                col_idx += c.overlap.get_col_width()

        # pprint.pprint(cols_per_level)
        # pprint.pprint(row_spans)
        # pprint.pprint(col_spans)

        col_spans = {x: x.get_col_width() for x in all_columns}

        return cols_per_level, end_columns, row_spans, col_spans, cols_matrix

    def _process_columns_levels(self, cols_matrix, cols_per_level, start, level):
        idx = start
        for col in self.columns:
            col_width = col.get_col_width()

            if col.name is not None:
                # cols_per_level[level].append(col)
                for i in range(col_width):
                    cols_matrix[level][idx + i] = ColumnEntry(overlap=col, user=col)
            else:
                for i in range(col_width):
                    cols_matrix[level][idx + i] = ColumnEntry(overlap=None, user=col)

            if col.is_column_group():
                col._process_columns_levels(cols_matrix, cols_per_level, idx, level + 1)
            idx += col_width

    def print(self):
        self._print(0)

    def _print(self, level):
        print("  " * level + (self.name or "(none)"))
        for c in self.columns:
            c._print(level + 1)

    def __repr__(self):
        return self.name or "(none)"


class ReportTableColumnGroup:
    def __init__(self, name: str, alignment: Alignment = None) -> None:
        self.name = name
        self.alignment = alignment
