import json
import datetime
from typing import Union
from enum import Enum
import copy

from dominate import tags
from markupsafe import Markup

from lib.html_tables.html_table import SortIcon, ColumnStyle, RowStyle
from lib.utils import DiffTracker
from .column import *
from .conditional_formatters import *
from .rows_group import *
from .summary import *
from .row import *
from .cell import *
from ..column_type import DisabledCell


class ReportTableDynamicData:
    def __init__(self):
        self.sort_column_tag = None  # type: str
        self.sort_dir = 0

    @staticmethod
    def parse(data: str):
        d = ReportTableDynamicData()
        p = data.split("-")
        d.sort_column_tag = p[0]
        d.sort_dir = int(p[1])
        return d


SummaryColFuncType = Callable[[ReportTableSummaryData], Tuple[Any, ColumnValueType]]


class ReportTableState(Enum):
    AddingHeaders = 0
    AddingRows = 1
    Finalized = 2


class ReportTable:
    def __init__(self, title: str = None, table_id: str = None, cls: str = None, multiple_row_groups=False,
                 is_dynamic=False) -> None:
        self.table_id = table_id
        self.title = title
        self.footer = ""
        self.cls = []  # type: List[str]
        if cls is not None:
            self.cls.append(cls)
        self.root_column = ReportTableColumn()
        self.rows_groups = []  # type: List[ReportTableRowsGroup]
        self.cur_row_group = None  # type: ReportTableRowsGroup
        self.cur_row = None  # type: ReportTableRow
        self.summary_rows = []  # type: List[ReportTableSummaryRowDesc]
        self.summary_cols = []  # type: List[ReportTableSummaryColumnDesc]

        self.multiple_row_groups = multiple_row_groups
        if not self.multiple_row_groups:
            self.cur_row_group = ReportTableRowsGroup(title=None)

        self.all_columns = None  # type: List[ReportTableColumn]
        self.dynamic_data = None  # type: ReportTableDynamicData
        self.is_dynamic = is_dynamic
        self.adding_columns = True
        self.border_columns = None

        self.title = title  # type: str
        self._freeze_header = False

        self.state = ReportTableState.AddingHeaders
        self.prev_state = None  # type: RowStyle

        self.row_num = 0

        self.sort_column_tag = None  # type: str
        self.sort_reverse = False

    def set_sort_column(self, tag: str, reverse: bool = False):
        self.sort_column_tag = tag
        self.sort_reverse = reverse

    def set_dynamic_data(self, data: ReportTableDynamicData):
        self.dynamic_data = data

    def parse_dynamic_data(self, data: Dict):
        if data is not None and self.table_id in data:
            self.dynamic_data = ReportTableDynamicData.parse(data[self.table_id])

    def set_title(self, title):
        self.title = title

    def set_footer(self, footer: str):
        self.footer = footer

    def freeze_header(self):
        self._freeze_header = True

    def add_column_group(self, name, hint=None):
        return self.root_column.add_column_group(name=name, hint=hint)

    def add_column_pos(self, name="#", hint=None, tag=None, alignment=Alignment.Center, width=50,
                       min_width=30) -> 'ReportTableColumn':
        return self.root_column.add_column_pos(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                               min_width=min_width)

    def add_column_text(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                        edit_info: ReportTableCellEditInfo = None) -> ReportTableColumn:
        return self.root_column.add_column_text(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                min_width=min_width, sortable=sortable,
                                                edit_info=edit_info)

    def add_column_boolean(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                           edit_info: ReportTableCellEditInfo = None) -> ReportTableColumn:
        return self.root_column.add_column_boolean(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                   min_width=min_width, sortable=sortable,
                                                   edit_info=edit_info)

    def add_column_date(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, sortable=False,
                        format: str = None,
                        formatter: IConditionalFormatter = None,
                        edit_info: ReportTableCellEditInfo = None) -> ReportTableColumn:
        return self.root_column.add_column_date(name=name, hint=hint, tag=tag, alignment=alignment, formatter=formatter,
                                                format=format, width=width,
                                                min_width=min_width,
                                                sortable=sortable,
                                                edit_info=edit_info)

    def add_column_number(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, hide_zero=True,
                          decimal_points=0,
                          formatter: IConditionalFormatter = None,
                          sortable=False, edit_info: ReportTableCellEditInfo = None) -> ReportTableColumn:
        return self.root_column.add_column_number(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                  min_width=min_width, hide_zero=hide_zero,
                                                  formatter=formatter, decimal_points=decimal_points,
                                                  sortable=sortable, edit_info=edit_info)

    def add_column_currency(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, hide_zero=True,
                            decimal_points=0,
                            formatter: IConditionalFormatter = None, sortable=False,
                            edit_info: ReportTableCellEditInfo = None) -> ReportTableColumn:
        return self.root_column.add_column_currency(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                    min_width=min_width, hide_zero=hide_zero,
                                                    decimal_points=decimal_points,
                                                    formatter=formatter, sortable=sortable, edit_info=edit_info)

    def add_column_percentage(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                              hide_zero=True, decimal_points=0,
                              formatter: IConditionalFormatter = None, sortable=False) -> ReportTableColumn:
        return self.root_column.add_column_percentage(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                      min_width=min_width, hide_zero=hide_zero,
                                                      decimal_points=decimal_points,
                                                      formatter=formatter, sortable=sortable)

    def add_column_duration(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                            hide_zero=True, format: str = None,
                            formatter: IConditionalFormatter = None, sortable=False) -> ReportTableColumn:
        return self.root_column.add_column_duration(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                    format=format,
                                                    min_width=min_width, hide_zero=hide_zero,
                                                    formatter=formatter, sortable=sortable)

    def add_column_diff(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None, decimal_points=0,
                        formatter: IConditionalFormatter = None, reverse=False) -> ReportTableColumn:
        return self.root_column.add_column_diff(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                min_width=min_width,
                                                decimal_points=decimal_points,
                                                formatter=formatter, reverse=reverse)

    def add_column_operations(self, name, hint=None, tag=None, alignment=None, width=None, min_width=None,
                              formatter: IConditionalFormatter = None) -> ReportTableColumn:
        return self.root_column.add_column_operations(name=name, hint=hint, tag=tag, alignment=alignment, width=width,
                                                      min_width=min_width, formatter=formatter)

    # def get_columns_groups(self) -> List[ReportTableColumnGroup]:
    #    return self.columns_groups

    def _check_state(self, state: ReportTableState):
        if self.state != state:
            raise ValueError("Incorrect state, expected: {0}, got: {1}".format(state, self.state))

    def _check_states(self, *states: ReportTableState):
        if self.state not in states:
            raise ValueError("Incorrect state, expected: {0}, got: {1}".format(states, self.state))

    def get_all_columns(self):
        self._check_state(ReportTableState.Finalized)
        return self.all_columns

    def get_columns(self) -> List[ReportTableColumn]:
        self._check_state(ReportTableState.Finalized)
        return self.root_column.columns

    def get_row_groups(self) -> List[ReportTableRowsGroup]:
        self._check_state(ReportTableState.Finalized)
        return self.rows_groups

    def new_row_group(self, title=None):
        if self.state == ReportTableState.AddingHeaders:
            self._finish_columns()

        self._check_state(ReportTableState.AddingRows)

        self._finish_row()
        self._finish_row_group()
        self.cur_row_group = ReportTableRowsGroup(title=title)

    def new_row(self, row_id: str = None, data: Any = None):
        if self.state == ReportTableState.AddingHeaders:
            self._finish_columns()

        self._check_state(ReportTableState.AddingRows)
        self._finish_row()
        self.cur_row = ReportTableRow(row_id, data)

        if self.root_column.columns[0].type == ColumnType.Position:
            self.add_cell(None)

    def remove_row(self):
        self.cur_row = None

    def add_cell(self, value, link=None, link_new_window=False, readonly=False) -> ReportTableCell:
        self._check_state(ReportTableState.AddingRows)
        column_idx = len(self.cur_row.values)
        cell = ReportTableCell(column=self.all_columns[column_idx], value=value, link=link,
                               link_new_window=link_new_window, readonly=readonly)
        # cell = ReportTableCell(column=None, value=value, link=link)
        self.cur_row.values.append(cell)
        return cell

    def add_empty_cell(self) -> ReportTableCell:
        self._check_state(ReportTableState.AddingRows)
        return self.add_cell(None)

    def add_group_summary_row(self, func: Callable[[ReportTableSummaryRowContext], Tuple[Any, ColumnValueType]],
                              title: str = None, colspan: int = 1):
        self._check_states(ReportTableState.AddingHeaders, ReportTableState.AddingRows, ReportTableState.Finalized)

        summary_row = ReportTableSummaryRowDesc(title=title, func=func, colspan=colspan)
        self.cur_row_group.summary_rows.append(summary_row)
        return summary_row

    def add_summary_row(self, func: Callable[[ReportTableSummaryRowContext], Tuple[Any, ColumnValueType]],
                        title: str = None, colspan: int = 1):
        self._check_states(ReportTableState.AddingHeaders, ReportTableState.AddingRows, ReportTableState.Finalized)

        summary_row = ReportTableSummaryRowDesc(title=title, func=func, colspan=colspan)
        self.summary_rows.append(summary_row)
        return summary_row

    def add_summary_row_for_tag(self, title: str, tag: Union[str, List[str]], colspan: int = 1):
        def s(ctx):
            if isinstance(tag, list):
                if ctx.tag in tag:
                    return ctx.data.sum()
            else:
                if ctx.tag == tag:
                    return ctx.data.sum()

        return self.add_summary_row(func=s, title=title, colspan=colspan)

    def add_group_summary_row_for_tag(self, title: str, tag: Union[str, List[str]], colspan: int = 1):
        def s(ctx):
            if isinstance(tag, list):
                if ctx.tag in tag:
                    return ctx.data.sum()
            else:
                if ctx.tag == tag:
                    return ctx.data.sum()

        self.add_group_summary_row(func=s, title=title, colspan=colspan)

    def add_summary_col(self, title: str, width: int, func: SummaryColFuncType):
        self._check_state(ReportTableState.AddingHeaders)

        col = ReportTableSummaryColumnDesc(title, width, func)
        self.summary_cols.append(col)

    def add_summary_col_for_tag(self, title: str, tag: str, width):
        def s(ctx: ReportTableSummaryData):
            return ctx.sum(tag), ColumnValueType.Number

        self.add_summary_col(func=s, title=title, width=width)

    def get_sum_by_tag(self, tag):
        self.finish()

        total = 0
        for i, c in enumerate(self.all_columns):
            if c.is_summary():
                continue

            if c.tag == tag:
                for rows_group in self.rows_groups:
                    for row in rows_group.rows:
                        val = c.get_raw_value(row.values[i])
                        total += val or 0
        return total

    def __str__(self):
        return str(self.render())

    def render(self):
        self.finish()

        depth = self.root_column.get_depth()
        cols_per_level, _, row_spans, col_spans, _ = self.root_column.get_leveled()
        end_columns = self.all_columns

        columns_fixed = all(x.is_fixed_width() for x in end_columns)
        # self.all_columns = end_columns

        total_columns = len(self.all_columns)
        columns_cls_map = {}

        for c in self.all_columns:
            columns_cls_map[c] = set()

        freezed_column_max = None
        for i, c in enumerate(end_columns):
            if c.freezed:
                freezed_column_max = i + c.get_col_width()
                break

        editables_data = {}

        for col in self.all_columns:
            if col.edit_info is not None:
                if col.tag is None:
                    raise ValueError("Column /{0}/ tag is None while using editable cells".format(col.name))

                d = col.edit_info.get_data()
                if d is not None:
                    editables_data[col.tag] = d

        table_data = {
            "freeze_header": self._freeze_header,
            "freeze_column": freezed_column_max,
        }

        self.border_columns = []

        with tags.table(id="table", cls=" ".join(["rtable"] + self.cls), data_table_data=json.dumps(table_data),
                        data_editables=json.dumps(editables_data)) as doc:
            if columns_fixed:
                total_width = sum(x.width for x in end_columns)
                tags.attr(width=total_width)
            else:
                tags.attr(width="100%")

            with tags.thead():
                if self.title is not None:
                    with tags.tr(cls="header-title"):
                        tags.td(self.title, colspan=total_columns)

                for level in range(depth):
                    with tags.tr(cls="header-row"):
                        first_summary = True
                        for col, col_idx in cols_per_level[level]:
                            col_type = ColumnStyle.Normal

                            if col.is_summary_style2():
                                if first_summary:
                                    col_type = ColumnStyle.SummaryFirst
                                else:
                                    col_type = ColumnStyle.Summary
                                first_summary = False

                            cls = [str(col_type.value)]

                            if col.border:
                                self.border_columns.append((col_idx, col_idx + col_spans[col] - 1))

                            if any(col_idx == x[0] for x in self.border_columns):
                                cls.append("group-start")
                            if any(col_idx + col_spans[col] - 1 == x[1] for x in self.border_columns):
                                cls.append("group-end")

                            if col in end_columns:
                                self._emit_header_cell(col, col_idx,
                                                       rowspan=row_spans[col], colspan=col_spans[col],
                                                       cls=cls, is_endcolumn=True)
                            else:
                                self._emit_header_cell(col, col_idx,
                                                       rowspan=row_spans[col], colspan=col_spans[col],
                                                       cls=cls, is_endcolumn=False)

            with tags.tbody():
                with tags.colgroup():
                    pass

                summary_cols_data = [ReportTableSummaryData() for _ in self.summary_cols]
                column_data_map = {x: ReportTableSummaryData() for x in self.all_columns}

                for row_group in self.rows_groups:
                    self._render_row_group(row_group, summary_cols_data, column_data_map)

                for summary_row in self.summary_rows:
                    self._emit_summary_row(
                            summary_row=summary_row,
                            column_data_map=column_data_map,
                            summary_cols_data=summary_cols_data)

            if len(self.footer) > 0:
                with tags.tfoot():
                    with tags.tr():
                        tags.td(self.footer, colspan=total_columns)

        return Markup(doc)

    def _emit_header_cell(self, col: ReportTableColumn, col_idx: int, rowspan, colspan, cls: List[str],
                          is_endcolumn: bool):
        if col.hint is not None:
            cls.append("show-tooltip")

        with tags.th(rowspan=rowspan, colspan=colspan, cls=list_to_cls(cls)) as th_el:

            c = col
            if c.sortable:
                if self.table_id is None:
                    raise ValueError("Sortable column detected but table has no id")
                if c.tag is None:
                    raise ValueError("Sortable column ({0}) without tag".format(c.name))

                if c.is_numeric():
                    sort_icon = SortIcon.NoSortNumber
                else:
                    sort_icon = SortIcon.NoSortText
                if self.dynamic_data is not None:
                    if self.dynamic_data.sort_column_tag == c.tag:
                        if self.dynamic_data.sort_dir == 0:
                            if c.is_numeric():
                                sort_icon = SortIcon.SortNumberAsc
                            else:
                                sort_icon = SortIcon.SortTextAsc
                        else:
                            if c.is_numeric():
                                sort_icon = SortIcon.SortNumberDesc
                            else:
                                sort_icon = SortIcon.SortTextDesc
            else:
                sort_icon = SortIcon.NoIcon

            if is_endcolumn and sort_icon != SortIcon.NoIcon:
                tags.div(col.name, cls="header-item-title", style="text-align: {0}".format(col.header_alignment.value))

                sort_dir = None
                with tags.div(cls="header-item-sort"):
                    icon_cls = []  # type: List[str]
                    if sort_icon == SortIcon.NoSortText:
                        icon_cls = ["fa-sort-alpha-asc"]
                        sort_dir = -1
                    if sort_icon == SortIcon.NoSortNumber:
                        icon_cls = ["fa-sort-numeric-asc"]
                        sort_dir = -1
                    if sort_icon == SortIcon.SortTextAsc:
                        icon_cls = ["fa-sort-alpha-asc", "sort-active"]
                        sort_dir = 0
                    if sort_icon == SortIcon.SortTextDesc:
                        icon_cls = ["fa-sort-alpha-desc", "sort-active"]
                        sort_dir = 1
                    if sort_icon == SortIcon.SortNumberAsc:
                        icon_cls = ["fa-sort-numeric-asc", "sort-active"]
                        sort_dir = 0
                    if sort_icon == SortIcon.SortNumberDesc:
                        icon_cls = ["fa-sort-numeric-desc", "sort-active"]
                        sort_dir = 1

                    with tags.i(cls=list_to_cls(["fa"] + icon_cls)):
                        pass

                    tags.attr(data_table_id=self.table_id)
                    tags.attr(data_sort_tag=col.tag)
                    tags.attr(data_sort_dir=sort_dir)
            else:
                th_el.add_raw_string(col.name)
                tags.attr(align=col.header_alignment.value)

            if col.width is not None:
                tags.attr(width=col.width)
            if col.min_width is not None:
                tags.attr(style="min-width: {0}px".format(col.min_width))
            if col.hint is not None:
                tags.attr(title=col.hint)
                th_el.add_raw_string("""<i class="fa fa-info info-hint" aria-hidden="true"></i>""")

    def _sort_rows(self, rows: List[ReportTableRow]) -> List[ReportTableRow]:
        if self.dynamic_data is not None:
            tag = self.dynamic_data.sort_column_tag
            sort_dir = self.dynamic_data.sort_dir

            def row_sort_key(row: ReportTableRow):
                col, col_idx = self._find_column_by_tag(tag)
                value = row.values[col_idx].value
                value = col.value_desc.get_raw_value_or_default(value)
                return value

            if sort_dir == 0:
                return sorted(rows, key=row_sort_key)
            else:
                return sorted(rows, key=row_sort_key, reverse=True)
        elif self.sort_column_tag is not None:
            tag = self.sort_column_tag

            def row_sort_key(row: ReportTableRow):
                col, col_idx = self._find_column_by_tag(tag)
                value = row.values[col_idx].value
                value = col.value_desc.get_raw_value_or_default(value)
                return value

            return sorted(rows, key=row_sort_key, reverse=self.sort_reverse)
        else:
            return rows

    def _render_row_group(self, row_group: ReportTableRowsGroup, summary_cols_data, column_data_map):
        self.prev_state = None

        row_group_summary_cols_data = [ReportTableSummaryData() for _ in self.summary_cols]
        row_group_column_data_map = {x: ReportTableSummaryData() for x in self.all_columns}

        if self.multiple_row_groups:
            with self._emit_row(RowStyle.Separator):
                self._emit_simple_cell(row_group.title, colspan=len(self.all_columns))

        # store data for DiffTracker based columns
        override_columns_data = {}  # type: Dict[Tuple[ReportTableColumn, ReportTableRow], Any]
        for i, c in enumerate(self.all_columns):
            if c.is_summary():
                continue

            tracker = DiffTracker()

            if c.value_desc is not None and c.value_desc.type == ColumnValueType.Diff:
                if c.value_desc.reverse:
                    rows = list(reversed(row_group.rows))
                else:
                    rows = row_group.rows

                for data_row in rows:
                    cell = data_row.values[i]
                    tracker.update(cell.value)
                    override_columns_data[(c, data_row)] = tracker.get_change_ratio()

        # process rows
        for data_row in self._sort_rows(row_group.rows):
            with self._emit_row(RowStyle.Normal):
                # process row data
                self.row_num += 1
                summary_row_data = ReportTableSummaryData()
                for i, c in enumerate(self.all_columns):
                    if c.is_summary():
                        continue

                    cls = [ColumnStyle.Normal.value if not c.is_summary_style2() else ColumnStyle.Summary.value]

                    if any(i == x[0] for x in self.border_columns):
                        cls.append("group-start")
                    if any(i == x[1] for x in self.border_columns):
                        cls.append("group-end")

                    raw_value = self._emit_cell(
                            cell=data_row.values[i],
                            row=data_row,
                            row_num=self.row_num,
                            override_columns_data=override_columns_data,
                            cls=cls
                    )
                    summary_row_data.add_cell(c.tag, raw_value)

                    row_group_column_data_map[c].add_cell(c.tag, raw_value)
                    column_data_map[c].add_cell(c.tag, raw_value)

                # process summary columns
                for sm_idx, summary_col in enumerate(self.summary_cols):
                    res = summary_col.func(summary_row_data)
                    if res is None:
                        self._emit_simple_cell("")
                    else:
                        value, value_desc = process_func_result(res, None)
                        value_desc.hide_zero = False
                        cls = [ColumnStyle.SummaryFirst.value if sm_idx == 0 else ColumnStyle.Summary.value]

                        if not value_desc.can_be_wrapped():
                            cls.append("nowrap")

                        self._emit_simple_cell(
                                value_desc.format_value(value),
                                alignment=Alignment.Right,
                                cls=cls)

                        # save summary cols values
                        summary_cols_data[sm_idx].add_cell(None, value_desc.get_raw_value(value))
                        row_group_summary_cols_data[sm_idx].add_cell(None, value_desc.get_raw_value(value))

        for summary_row in row_group.summary_rows:
            self._emit_summary_row(
                    summary_row=summary_row,
                    column_data_map=row_group_column_data_map,
                    summary_cols_data=row_group_summary_cols_data)

    def _emit_summary_row(self, summary_row: ReportTableSummaryRowDesc, column_data_map, summary_cols_data):
        with self._emit_row(RowStyle.Summary):
            to_skip = 0

            summary_row_data = ReportTableSummaryData()
            for i, c in enumerate(self.all_columns):
                if c.is_summary():
                    continue

                cell_data = ""
                cls = []

                if to_skip > 0:
                    to_skip -= 1
                    continue

                if i == 0 and summary_row.title is not None:
                    self._emit_simple_cell(summary_row.title, colspan=summary_row.colspan)
                    to_skip = (summary_row.colspan or 0) - 1
                    continue
                res = summary_row.func(ReportTableSummaryRowContext(i, c.tag, column_data_map[c]))
                if res is None:
                    summary_row_data.add_cell(c.tag, None)
                else:
                    value, value_desc = process_func_result(res, c.value_desc)
                    value_desc.hide_zero = False
                    cell_data = value_desc.format_value(value)
                    summary_row_data.add_cell(c.tag, value_desc.get_raw_value(value))

                    if not value_desc.can_be_wrapped():
                        cls.append("nowrap")

                self._emit_simple_cell(cell_data, alignment=Alignment.Right, cls=cls)

            # process summary corners
            for sm_idx, _ in enumerate(self.summary_cols):
                cell_data = ""
                cls = [ColumnStyle.SummaryFirst.value if sm_idx == 0 else ColumnStyle.Summary.value]
                if sm_idx < len(summary_row.corner_funcs) is not None:
                    res = summary_row.corner_funcs[sm_idx](summary_row_data, summary_cols_data[sm_idx])
                    if res is not None:
                        value, value_desc = process_func_result(res, None)
                        value_desc.hide_zero = False
                        cell_data = value_desc.format_value(value)

                        if not value_desc.can_be_wrapped():
                            cls.append("nowrap")

                self._emit_simple_cell(cell_data, alignment=Alignment.Right, cls=cls)

    def finish(self):
        if self.state == ReportTableState.AddingHeaders:
            self._finish_columns()
        if self.state == ReportTableState.AddingRows:
            self._finish_row()
            self._finish_row_group()
        self.state = ReportTableState.Finalized

    def get_column_title(self, num: int):
        parts = []
        for lvl in reversed(range(len(self._all_columns_matrix))):
            c = self._all_columns_matrix[lvl][num]
            if c.user is not None and c.user.name is not None and len(c.user.name) > 0:
                parts.append(c.user.name)
        return " / ".join(reversed(parts))

    def _finish_columns(self):
        self._check_state(ReportTableState.AddingHeaders)

        for c in self.summary_cols:
            q = self.root_column.add_column_text(c.title, alignment=Alignment.Right, width=c.width)
            q.is_summary_col = True

        _, self.all_columns, _, _, self._all_columns_matrix = self.root_column.get_leveled()
        # self.all_columns = self.root_column.get_all_columns()

        self.state = ReportTableState.AddingRows

    def _finish_row(self):
        self._check_state(ReportTableState.AddingRows)

        if self.cur_row is not None:
            self.cur_row_group.rows.append(self.cur_row)
            self.cur_row = None

    def _finish_row_group(self):
        self._check_state(ReportTableState.AddingRows)

        if self.cur_row_group is not None:
            self.rows_groups.append(self.cur_row_group)
            self.cur_row_group = None

    def _find_column_by_tag(self, tag: str):
        idx = 0
        for c in self.all_columns:
            if c.tag == tag:
                return c, idx
            idx += 1

    def _emit_row(self, style):
        state_map = {
            (None, RowStyle.Normal): RowStyle.NormalEven,
            (RowStyle.Separator, RowStyle.Normal): RowStyle.NormalEven,
            (RowStyle.NormalEven, RowStyle.Normal): RowStyle.NormalOdd,
            (RowStyle.NormalOdd, RowStyle.Normal): RowStyle.NormalEven,

            (None, RowStyle.Separator): RowStyle.Separator,

            (None, RowStyle.Summary): RowStyle.SummaryFirst,
            (RowStyle.NormalEven, RowStyle.Summary): RowStyle.SummaryFirst,
            (RowStyle.NormalOdd, RowStyle.Summary): RowStyle.SummaryFirst,
            (RowStyle.SummaryFirst, RowStyle.Summary): RowStyle.Summary,
            (RowStyle.Separator, RowStyle.Summary): RowStyle.Summary,

            (RowStyle.Summary, RowStyle.Summary): RowStyle.Summary,
        }

        next_style = state_map[(self.prev_state, style)]

        with tags.tr() as e:
            if next_style is not None:
                tags.attr(cls=next_style.value)

        self.prev_state = next_style

        return e

    def _emit_simple_cell(self,
                          text: str,
                          colspan: int = 1,
                          alignment: Alignment = Alignment.Left,
                          cls: Union[str, List[str]] = None):
        if isinstance(cls, list):
            cls = list_to_cls(cls)
        with tags.td(text, colspan=colspan, align=alignment.value):
            if cls is not None:
                tags.attr(cls=cls)

    def _emit_cell(self,
                   cell: ReportTableCell,
                   row: ReportTableRow,
                   row_num: int,
                   override_columns_data=None,
                   cls=None):

        if cls is None:
            cls = []

        column = cell.column
        styles = {}

        if column.type == ColumnType.Value:
            value = cell.value
            if (column, row) in override_columns_data:
                value = override_columns_data[(column, row)]

            raw_value = column.get_raw_value(value)
            if column.formatter is not None:
                style = column.formatter.format(raw_value, row)
            else:
                style = HtmlTableCellStyle()
            if cell.onclick_handler is not None:
                style.cursor = "pointer"

            display_value = cell.column.format_value(value)
            alignment = cell.column.alignment or cell.column.value_desc.get_default_alignment()

            if style.background_color is not None:
                if style.background_color_alt is None:
                    bg = style.background_color.hex_l
                else:
                    # if not self.even_row:
                    #    bg = style.background_color.hex_l
                    # else:
                    # TODO: implement
                    bg = style.background_color_alt.hex_l
                styles["background-color"] = bg
            if style.cursor is not None:
                styles["cursor"] = style.cursor

            if not cell.column.value_desc.can_be_wrapped():
                cls.append("nowrap")

        elif column.type == ColumnType.Position:
            raw_value = None
            display_value = str(row_num)
            alignment = cell.column.alignment or Alignment.Center
        else:
            raw_value = None
            display_value = ""
            alignment = cell.column.alignment or Alignment.Center

        if not isinstance(display_value, str):
            raise ValueError(
                    "cell (column: {0}) value should be str, got: {1} of type {2}".format(cell.column.name,
                                                                                          display_value,
                                                                                          type(display_value)))

        cnt = Markup(display_value.replace("\0", "&nbsp;").replace("\n", "<br/>"))

        table_id = str(self.table_id)
        row_id = str(row.id)
        tag = str(cell.column.tag)

        with tags.td(align=alignment.value, cls=list_to_cls(cls)) as td:
            if cell.column.width is not None:
                tags.attr(width=cell.column.width)
            if styles:
                tags.attr(style=map_to_style(styles))

            if cell.column.type == ColumnType.Operation:
                for _op in cell.value:
                    op = _op  # type: ReportTableOperationButton
                    if op.link is not None:
                        tags.a(op.name,
                               cls="editable-btn btn btn-default btn-xs",
                               href=op.link)
                    else:
                        tags.div(op.name,
                                 cls="editable-btn btn btn-default btn-xs",
                                 data_action=op.action,
                                 data_type="op",
                                 data_column_tag=op.tag,
                                 data_table_id=table_id,
                                 data_row_id=row_id,
                                 data_confirm=1 if op.confirm else 0,
                                 data_prompt=op.prompt,
                                 data_value="1")

            else:
                # Normal cell (noneditable)
                if cell.column.edit_info is None or cell.readonly:  # TODO: check in server ajax edit req
                    if cell.link is None:
                        td.add_raw_string(Markup(cnt))

                        if cell.onclick_handler is not None:
                            td.add_raw_string(
                                    """<i class="fa fa-hand-pointer-o clickable-hint" aria-hidden="true"></i>""")
                    else:
                        with tags.a(Markup(cnt), href=cell.link):
                            if cell.link_new_window is True:
                                tags.attr(target="_blank")

                    if cell.tooltip:
                        tags.attr(data_tooltip_content_html=cell.tooltip)

                # Editable cell
                else:
                    if row.id is None:
                        raise ValueError("Empty row id while using editable cells")

                    if cell.value == DisabledCell:
                        tags.div()
                    else:
                        if isinstance(cell.value, datetime.date):
                            data = json.dumps(cell.value.strftime("%m/%d/%Y"))
                        else:
                            data = json.dumps(cell.value)
                        tags.div(data,
                                 cls="editable",
                                 data_action=cell.column.edit_info.action,
                                 data_type=cell.column.edit_info.type.value,
                                 data_column_tag=tag,
                                 data_table_id=table_id,
                                 data_row_id=row_id)

                if cell.onclick_handler is not None:
                    tags.attr(onclick=cell.onclick_handler)

        return raw_value


def process_func_result(res, default_desc: Optional[ValueDesc]) -> Tuple[Any, ValueDesc]:
    if not isinstance(res, tuple):
        value = res
        value_desc_hint = None
    elif len(res) == 2:
        value = res[0]
        value_desc_hint = res[1]
    else:
        raise ValueError("invalid number of func output values")

    if value_desc_hint is None:
        if default_desc is None:
            raise ValueError("unspecified column type")
        value_desc = default_desc
    elif isinstance(value_desc_hint, ColumnValueType):
        value_desc = ValueDesc(type=value_desc_hint)
    elif isinstance(value_desc_hint, ValueDesc):
        value_desc = value_desc_hint
    else:
        raise ValueError("unsupported value type: {0}, {1}".format(type(value_desc_hint), value_desc_hint))

    return value, copy.copy(value_desc)


def map_to_style(d):
    return "; ".join("{0}: {1}".format(k, v) for k, v in d.items())


def list_to_cls(d):
    return " ".join(d)
