from typing import List, TYPE_CHECKING, Any

if TYPE_CHECKING:
    from .cell import ReportTableCell


class ReportTableRow:
    def __init__(self, id: str = None, data: Any = None) -> None:
        self.id = id
        self.data = data
        self.values = []  # type: List[ReportTableCell]

    def get_cells(self) -> List['ReportTableCell']:
        return self.values
