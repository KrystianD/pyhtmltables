from typing import TYPE_CHECKING
from decimal import Decimal

import abc
import colour

import lib.utils
from lib.html_tables.html_table import HtmlTableCellStyle

if TYPE_CHECKING:
    from .row import ReportTableRow


class IConditionalFormatter:
    @abc.abstractmethod
    def format(self, raw_value, row: 'ReportTableRow') -> HtmlTableCellStyle:
        pass


class RangeScaleFormatter(IConditionalFormatter):
    def __init__(self, value_min, value_center, value_max, color_min, color_center, color_max) -> None:
        self.value_min = value_min
        self.value_center = value_center
        self.value_max = value_max
        self.color_min = color_min
        self.color_center = color_center
        self.color_max = color_max

    def format(self, raw_value, row: 'ReportTableRow') -> HtmlTableCellStyle:
        style = HtmlTableCellStyle()

        if raw_value is None:
            return style

        if isinstance(raw_value, Decimal):
            raw_value = float(raw_value)

        if raw_value < self.value_min:
            raw_value = self.value_min
        if raw_value > self.value_max:
            raw_value = self.value_max

        if raw_value <= self.value_center:
            ratio = (raw_value - self.value_min) / (self.value_center - self.value_min)
            style.background_color_alt = style.background_color = lib.utils.interpolate_color(self.color_min,
                                                                                              self.color_center, ratio)
        else:
            ratio = (raw_value - self.value_center) / (self.value_max - self.value_center)
            style.background_color_alt = style.background_color = lib.utils.interpolate_color(self.color_center,
                                                                                              self.color_max, ratio)

        return style


class PercentageScaleFormatter(RangeScaleFormatter):
    def __init__(self) -> None:
        green = colour.Color("green")
        white = colour.Color("white")
        red = colour.Color("red")
        super().__init__(0, 0.5, 1, red, white, green)
