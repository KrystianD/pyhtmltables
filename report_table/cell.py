from typing import Dict, TYPE_CHECKING

if TYPE_CHECKING:
    from .column import ReportTableColumn


class ReportTableCell:
    def __init__(self, column: 'ReportTableColumn', value=None, link=None, link_new_window=None,
                 readonly=False) -> None:
        self.column = column
        self.value = value
        self.onclick_handler = None  # type: str
        self.custom_data = {}  # type: Dict[str,str]
        self.link = link  # type: str
        self.link_new_window = link_new_window  # type: bool
        self.readonly = readonly  # type: bool
        self.tooltip = None

    def get_value(self):
        return self.value

    def add_custom_data(self, key: str, value: str):
        self.custom_data[key] = value

    def set_onclick_handler(self, code: str):
        self.onclick_handler = code

    def set_link(self, link, new_window=False):
        self.link = link
        self.link_new_window = new_window
