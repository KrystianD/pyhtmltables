from typing import Any, Callable, Tuple, List, TYPE_CHECKING

import lib.utils

if TYPE_CHECKING:
    from lib.html_tables.column_value_type import ColumnValueType


class ReportTableSummaryDataCell:
    def __init__(self, column_tag, value) -> None:
        self.column_tag = column_tag
        self.value = value

    def __str__(self):
        return "[{0}, {1}]".format(self.column_tag, self.value)


class ReportTableSummaryData:
    def __init__(self) -> None:
        self.cells = []  # type: List[ReportTableSummaryDataCell]

    def add_cell(self, tag, value):
        self.cells.append(ReportTableSummaryDataCell(tag, value))

    def get_values(self, tag=None, skip_none=False):
        cells = []  # type: List[Any]
        for x in self.cells:
            if tag is not None and x.column_tag != tag:
                continue
            if skip_none and x.value is None:
                continue
            cells.append(x.value)
        return cells

    def sum(self, tag=None):
        return sum(self.get_values(tag=tag, skip_none=True))

    def min(self, tag=None):
        return lib.utils.mymin(self.get_values(tag=tag, skip_none=True))

    def max(self, tag=None):
        return lib.utils.mymax(self.get_values(tag=tag, skip_none=True))

    def median(self, tag=None):
        return lib.utils.median(self.get_values(tag=tag, skip_none=True))

    def average(self, tag=None):
        return lib.utils.average(self.get_values(tag=tag, skip_none=True))

    def __str__(self):
        return str(",".join(str(x) for x in self.cells))


class ReportTableSummaryRowContext:
    def __init__(self, idx: int, tag: str, data: ReportTableSummaryData) -> None:
        self.idx = idx
        self.tag = tag
        self.data = data


CornerFuncType = Callable[[ReportTableSummaryData, ReportTableSummaryData], Tuple[Any, 'ColumnValueType']]


class ReportTableSummaryRowDesc:
    def __init__(self,
                 title: str = None,
                 func: Callable[[ReportTableSummaryRowContext], Tuple[Any, 'ColumnValueType']] = None,
                 colspan: int = None) -> None:
        self.title = title
        self.func = func
        self.colspan = colspan
        self.corner_funcs = []  # type:List[CornerFuncType]

    def add_corner(self, func: CornerFuncType = None):
        self.corner_funcs.append(func)


class ReportTableSummaryColumnDesc:
    def __init__(self, title: str, width: int,
                 func: Callable[[ReportTableSummaryData], Tuple[Any, 'ColumnValueType']]) -> None:
        self.title = title
        self.width = width
        self.func = func
