from lib.html_tables.column_type import DisabledCell
from lib.utils import ComparableMixin


class Ratio(ComparableMixin):
    def __init__(self, a, b) -> None:
        self.a = a
        self.b = b

    def is_valid(self):
        a_not_none = self.a is not None and self.a != DisabledCell
        b_not_none = self.b is not None and self.b != DisabledCell
        return a_not_none and b_not_none and self.b != 0

    def get_display_value_or_none(self):
        return self.a / self.b * 100 if self.is_valid() else None

    def get_raw_value_or_none(self):
        return self.a / self.b if self.is_valid() else None

    def __repr__(self):
        return "{0}/{1} = {2}".format(self.a, self.b, self.get_raw_value_or_none())

    def __lt__(self, other: 'Ratio'):
        v1 = self.get_raw_value_or_none() or 0
        v2 = other.get_raw_value_or_none() or 0
        return v1 < v2
