import base64
import datetime
import json
import re
from typing import List, Any, Union

import enum
import shortuuid
from markupsafe import Markup


class Code:
    def __init__(self, code):
        self.code = code


def jseval(code: str):
    return "EVAL:" + base64.b64encode(code.encode("ascii")).decode("ascii") + ":EVAL"


class CodeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Code):
            return "A"

        return json.JSONEncoder.default(self, obj)


class ChartType(enum.Enum):
    Line = "line"
    Bar = "bar"


class ChartSeries:
    def __init__(self):
        self.name: str = None
        self.values: List[Any] = []
        self.is_secondary = False

    def set_secondary(self):
        self.is_secondary = True

    def add_value(self, value: Any):
        self.values.append(value)
        return self

    def prepend_value(self, value: Any):
        self.values.insert(0, value)
        return self


class AxisTypeEnum(enum.Enum):
    Number = 0
    Currency = 1


class SimpleChart:
    def __init__(self, type: ChartType):
        self.type = type
        self.labels: List[str] = []
        self.series: List[ChartSeries] = []
        self.is_horizontal = False
        self.is_timeseries = False
        self.primary_axis = AxisTypeEnum.Number
        self.secondary_axis = AxisTypeEnum.Number

    def set_primary_axis_type(self, type: AxisTypeEnum):
        self.primary_axis = type

    def set_secondary_axis_type(self, type: AxisTypeEnum):
        self.secondary_axis = type

    def set_horizontal(self):
        self.is_horizontal = True

    def set_timeseries(self):
        self.is_timeseries = True

    def add_label(self, name: Union[str, datetime.date]):
        self.labels.append(name)

    # def add_datetime_label(self, name: datetime.date):
    #     self.labels.append(name)

    def prepend_label(self, name: str):
        self.labels.insert(0, name)

    def add_series(self, name: str) -> ChartSeries:
        s = ChartSeries()
        s.name = name
        self.series.append(s)
        return s

    def render(self):
        id = "id" + shortuuid.uuid()

        def convert_label(x):
            if self.is_timeseries:
                assert isinstance(x, datetime.date)
                return x.strftime("%Y-%m-%d")
            else:
                assert isinstance(x, str)
                return x

        columns = [
            ["x", *[convert_label(x) for x in self.labels]]
        ]
        for series in self.series:
            cd = [series.name] + series.values
            columns.append(cd)

        has_secondary = any(x.is_secondary for x in self.series)

        def get_axis_format(type: AxisTypeEnum):
            if type == AxisTypeEnum.Currency:
                return jseval("d3.format('$,')")
            if type == AxisTypeEnum.Number:
                return jseval("d3.format(',')")

        def create_xaxis():
            if self.is_timeseries:
                return {
                    "type": 'timeseries',
                    "tick": {
                        "format": "%m/%d/%Y"
                    }
                }
            else:
                return {
                    "type": 'category'
                }

        config = {
            "bindto": '#' + id,
            "data": {
                "x": "x",
                "columns": columns,
                "type": self.type.value,
                "axes": {x.name: "y2" if x.is_secondary else "y" for x in self.series},
            },
            "legend": {
                "position": 'right'
            },
            "axis": {
                "rotated": self.is_horizontal,
                "x": create_xaxis(),
                "y": {
                    "show": True,
                    "tick": {
                        "format": get_axis_format(self.primary_axis)
                    }
                },
                "y2": {
                    "show": has_secondary,
                    "tick": {
                        "format": get_axis_format(self.secondary_axis)
                    }
                }
            },
            "grid": {
                # "y": {
                #    "lines": [{value:0}]
                # }
                "x": {
                    "show": True
                }
            }
        }

        html = """
<div id="{id}"></div>
<script>
var chart = c3.generate({config});
</script>
""".format(id=id, config=json.dumps(config, indent=2, cls=CodeEncoder))

        html = re.sub("\"EVAL:(.*?):EVAL\"", lambda m: base64.b64decode(m.group(1)).decode("ascii"), html)

        return Markup(html)
