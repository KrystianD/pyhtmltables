from enum import Enum


class ColumnValueType(Enum):
    Text = 0
    Number = 1
    Currency = 2
    Percentage = 3
    Date = 4
    Diff = 5
    Link = 6
    Boolean = 7
    Duration = 8
